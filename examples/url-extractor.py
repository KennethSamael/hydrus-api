#!/usr/bin/env python3

# Copyright (C) 2021 cryzed
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import argparse
import re

import hydrus_api
import hydrus_api.utils

argument_parser = argparse.ArgumentParser()
argument_parser.add_argument("api_key")
argument_parser.add_argument("regex")
argument_parser.add_argument("tags", nargs="+")
argument_parser.add_argument("--chunk-size", "-c", type=int, default=100)
argument_parser.add_argument("--output", "-o", default="urls.txt")
argument_parser.add_argument("--api-url", "-a", default=hydrus_api.DEFAULT_API_URL)

ERROR_EXIT_CODE = 1
REQUIRED_PERMISSIONS = {hydrus_api.Permission.SEARCH_FILES}


def main(arguments):
    client = hydrus_api.Client(arguments.api_key, arguments.api_url)
    if not hydrus_api.utils.verify_permissions(client, REQUIRED_PERMISSIONS):
        print("The API key does not grant all required permissions:", REQUIRED_PERMISSIONS)
        return ERROR_EXIT_CODE

    regex = re.compile(arguments.regex)
    output = open(arguments.output, "w", encoding=hydrus_api.HYDRUS_METADATA_ENCODING)

    file_ids = client.search_files(arguments.tags)
    for file_ids in hydrus_api.utils.yield_chunks(file_ids, arguments.chunk_size):
        metadata = client.get_file_metadata(file_ids=file_ids)
        for metadatum in metadata:
            for url in metadatum["known_urls"]:
                if regex.match(url):
                    print(url)
                    output.write(url + "\n")


if __name__ == "__main__":
    arguments = argument_parser.parse_args()
    argument_parser.exit(main(arguments))
