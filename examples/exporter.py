#!/usr/bin/env python3

# Copyright (C) 2021 cryzed
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import argparse
import os

import hydrus_api
import hydrus_api.utils

ERROR_EXIT_CODE = 1
REQUIRED_PERMISSIONS = {hydrus_api.Permission.SEARCH_FILES}

argument_parser = argparse.ArgumentParser()
argument_parser.add_argument("api_key")
argument_parser.add_argument("tags", nargs="+")
argument_parser.add_argument("--or-search", "-o", action="store_true")
argument_parser.add_argument("--service", "-s", dest="services", action="store", default=("my tags",))
argument_parser.add_argument("--write-metadata", "-m", action="store_true")
argument_parser.add_argument("--limit", "-l", type=int, default=None)
argument_parser.add_argument("--destination", "-d", default=os.getcwd())
argument_parser.add_argument("--chunk-size", "-c", type=int, default=100)
argument_parser.add_argument("--api-url", "-a", default=hydrus_api.DEFAULT_API_URL)


def main(arguments):
    client = hydrus_api.Client(arguments.api_key, arguments.api_url)
    if not hydrus_api.utils.verify_permissions(client, REQUIRED_PERMISSIONS):
        print("The API key does not grant all required permissions:", REQUIRED_PERMISSIONS)
        return ERROR_EXIT_CODE

    all_file_ids = set()
    if arguments.or_search:
        for tag in arguments.tags:
            all_file_ids.update(client.search_files([tag]))
    else:
        all_file_ids.update(client.search_files(arguments.tags))

    exported = 0
    print(f"Exporting {arguments.limit or len(all_file_ids)} files to {arguments.destination}...")
    for file_ids in hydrus_api.utils.yield_chunks(list(all_file_ids), arguments.chunk_size):
        metadata = client.get_file_metadata(file_ids=file_ids)
        for metadatum in metadata:
            if arguments.limit is not None and exported >= arguments.limit:
                return

            mime_type = metadatum["mime"]
            extension = "." + mime_type.rsplit("/", 1)[1] if "/" in mime_type else ""
            path = os.path.join(arguments.destination, metadatum["hash"] + extension)
            with open(path, "wb") as file:
                file.write(client.get_file(file_id=metadatum["file_id"]).content)

            if arguments.write_metadata:
                tags = set()
                services_to_statuses = metadatum["service_names_to_statuses_to_tags"]
                for service_name in set(arguments.services):
                    service = services_to_statuses.get(service_name)
                    if service:
                        # We have to convert the enums integer value explicitly to a string, because JSON doesn't
                        # support integer keys, so the dict we are getting back is full of string keys
                        current_tags = service.get(str(hydrus_api.TagStatus.CURRENT))
                        if current_tags:
                            tags.update(current_tags)

                with open(path + ".txt", "w", encoding=hydrus_api.utils.HYDRUS_METADATA_ENCODING) as file:
                    file.write("\n".join(sorted(tags)))

            exported += 1


if __name__ == "__main__":
    arguments = argument_parser.parse_args()
    argument_parser.exit(main(arguments))
