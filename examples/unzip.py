#!/usr/bin/env python3

# Copyright (C) 2021 cryzed
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import argparse
import enum
import logging
import logging.handlers
import pathlib
import shutil
import tempfile
import zipfile

import hydrus_api
import hydrus_api.utils

logger = logging.getLogger("hydrus_api.examples.unzip")

PERMISSIONS = {hydrus_api.Permission.IMPORT_FILES, hydrus_api.Permission.ADD_TAGS}


class ExitCode(enum.IntEnum):
    SUCCESS = 0
    FAILURE = 1


def get_argument_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("access_key")
    parser.add_argument("--api-url", "-a", default=hydrus_api.DEFAULT_API_URL)
    parser.add_argument("--search-tag", "-t", dest="search_tags", action="append", default=["system:filetype is zip"])
    parser.add_argument("--service-name", "-s", default="my tags")
    parser.add_argument(
        "--log-level", choices={"CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG", "NOTSET"}, default="INFO"
    )
    return parser


def run(arguments: argparse.Namespace) -> ExitCode:
    logger.info("Started")
    client = hydrus_api.Client(arguments.access_key, arguments.api_url)
    if not hydrus_api.utils.verify_permissions(client, PERMISSIONS):
        logger.error("The API key does not grant all required permissions: %s", PERMISSIONS)
        return ExitCode.FAILURE

    zip_path = pathlib.Path(tempfile.mkstemp()[1])
    for file_id in client.search_files(arguments.search_tags):
        metadata = client.get_file_metadata(file_ids=[file_id])[0]
        service_names_to_statuses_to_tags = metadata["service_names_to_statuses_to_tags"]
        tags = service_names_to_statuses_to_tags[arguments.service_name][str(hydrus_api.TagStatus.CURRENT)]

        logger.info("Downloading %s", metadata["hash"])
        response = client.get_file(file_id=file_id)
        zip_path.write_bytes(response.content)
        zip_dir = pathlib.Path(tempfile.mkdtemp())
        logger.info("Extracting...")
        with zipfile.ZipFile(zip_path) as file:
            file.extractall(zip_dir)

        file_paths = tuple(path for path in zip_dir.rglob("*") if path.is_file())
        logger.info("Adding and tagging %d files", len(file_paths))
        client.add_and_tag_files(file_paths, tags, [arguments.service_name])

        logger.debug("Removing temp dir %r", str(zip_dir))
        shutil.rmtree(zip_dir, ignore_errors=True)

    logger.debug("Removing temp file %r", str(zip_path))
    zip_path.unlink(missing_ok=True)
    return ExitCode.SUCCESS


def main() -> None:
    parser = get_argument_parser()
    arguments = parser.parse_args()

    logging.basicConfig(format="%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    logger.setLevel(arguments.log_level)

    try:
        parser.exit(run(arguments))
    except Exception:
        logger.critical("Error", exc_info=True)
        parser.exit(ExitCode.FAILURE)


if __name__ == "__main__":
    main()
